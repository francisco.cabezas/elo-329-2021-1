import cl.telematica.elo329.entities.Curso;
import cl.telematica.elo329.entities.Elo329;
import cl.telematica.elo329.entities.Estudiante;
import cl.telematica.elo329.entities.Persona;

/**
 * int main(int argc, char[] argv) {}
 * char getValue() {}
 * cout >>
 * print
 * printf()
 * console.log
 */

public class Main {

    public static void main(String[] args) {
        // System
        //    |-> out (PrintStream)
        //          |-> println(String x) -> método

        // javac Main.java --> Main.class --> Lenguaje de máquina
        // java Main

        // Objeto --> Es una instancia de una clase
        // new Persona("Francisco", 35);
        Persona persona1 = new Persona();
        persona1.setName("Javier");
        persona1.setAge(40);

        System.out.println(persona1.getBasicInfo());

        Estudiante arnaldo = new Estudiante("Arnaldo", 35, "ELO-329");
        int[] arnaldoScores = {10, 70, 80};
        arnaldo.setScores(arnaldoScores);

        System.out.println(arnaldo.getBasicInfo());
        System.out.println(arnaldo.getStudentData());
        System.out.println("Promedio: " + arnaldo.calculateAverageScore());

        Estudiante leonor = new Estudiante("Leonor", 25, "ELO-329");
        int[] leonorScores = {80, 30, 55};
        leonor.setScores(leonorScores);

        System.out.println(leonor.getBasicInfo());
        System.out.println(leonor.getStudentData());
        System.out.println("Promedio: " + leonor.calculateAverageScore());

        Curso elo329 = new Elo329("ELO-329", "Electrónica");

        System.out.println("ARNALDO: " + elo329.getStudentSituation(arnaldo));
        System.out.println("LEONOR: " + elo329.getStudentSituation(leonor));
    }

}
