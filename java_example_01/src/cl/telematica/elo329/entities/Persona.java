package cl.telematica.elo329.entities;

// Encapsulamiento -> los atributos deben ser modificados solo por la clase
public class Persona {
    private String name;  // Métodos accesores -> get (), set ()
    private int age; // Patrón Builder

    public Persona() {
        name = "";
        age = 0;
    }

    public Persona(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getBasicInfo() {
        return "My name is " + this.name + ", and my age is: " + this.age;
    }
}
