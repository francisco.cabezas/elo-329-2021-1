package cl.telematica.elo329.entities;

public class Estudiante extends Persona {
    private String curso;
    private int[] scores;  // private int[] scores = { 1, 2, 3 }; scores[0] = 1, scores[1] = 2, scores[2] = 3;

    public Estudiante() {
        super();
        curso = "";
    }

    public Estudiante(String name, int age, String curso) {
        super(name, age);
        this.curso = curso;
    }

    public void setScores(int[] scores) {
        this.scores = scores;
    }

    /**
     * Tipos de iteradores:
     *
     * for (int i = 0; i < size; i++) {
     *      // sum += this.scores[i];
     *      sum = sum + this.scores[i];
     * }
     *
     * int i = 0;
     * while (i < size) {
     *      sum = sum + this.scores[i];
     *      i++;
     * }
     *
     * @return
     */
    public float calculateAverageScore() { // Sumatoria de cada elemento / tamaño
        if (this.scores == null || this.scores.length == 0) return 0;
        int size = this.scores.length;
        int sum = 0;
        for (int score : this.scores) {
            sum = sum + score;
        }
        return sum / size;
    }

    public String getStudentData() {
        return "Student name: " + super.getName() + ", my age: " + super.getAge() + ", my course: " + this.curso;
    }
}
