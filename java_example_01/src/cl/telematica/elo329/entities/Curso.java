package cl.telematica.elo329.entities;

public class Curso {
    private String sigla;
    private String departamento;

    public Curso(String sigla, String departamento) {
        this.sigla = sigla;
        this.departamento = departamento;
    }

    /**
     * Si promedio de notas >= 55 => APROBADO, sino REPROBADO
     * @param student
     * @return
     */
    public String getStudentSituation (Estudiante student) {
        int avrScore = Math.round(student.calculateAverageScore()); // Redondear la nota, ej: 54,5 = 55
        if (avrScore >= 55) {
            return "APROBADO";
        } else {
            return "REPROBADO";
        }
    }
}
