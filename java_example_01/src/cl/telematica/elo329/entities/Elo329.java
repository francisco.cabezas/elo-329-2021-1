package cl.telematica.elo329.entities;

public class Elo329 extends Curso {

    public Elo329(String sigla, String departamento) {
        super(sigla, departamento);
    }

    @Override
    public String getStudentSituation (Estudiante student) {
        int avrScore = Math.round(student.calculateAverageScore());
        if (avrScore >= 55) {
            return "APROBADO";
        } else if (avrScore >= 50 && avrScore < 55) {
            return "GLOBAL";
        } else {
            return "REPROBADO";
        }
    }
}
